<?php
$link = mysqli_connect("localhost","username","password","sample");

if(!$link){
  echo "Error in Connection";
  exit;
}

$sql = "SELECT userID,accessToken FROM clientauth";
$searchData = array();

$result = mysqli_query($link,$sql);
if (mysqli_num_rows($result) > 0) {
  while($row = mysqli_fetch_assoc($result)) {
      $data["accessToken"] = $row['accessToken'];
      $data["userID"] = $row['userID'];
      array_push($searchData,$data);
  }
}
echo json_encode($searchData);

?>
