  window.fbAsyncInit = function() {
    FB.init({
      appId            : '461031487743502',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v3.1'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

$("#fb-share-button").click(function(){
  FB.getLoginStatus(function(response) {
    console.log(response);
    statusChangeCallback(response);
  });
});

function statusChangeCallback(response){
  var response_value = response;
  if(response_value.status != undefined){
    if(response_value.status == "connected"){
      $("#login_status").val("connected");
      var response_auth = response_value.authResponse;
      $("#userID").val(response_auth.userID);
      $("#accessToken").val(response_auth.accesstoken);
    }
    else{
      $("#login_status").val("Couldn't Able to Connect");
    }
  }
  else{
    $("#login_status").val("Couldn't Able to Connect");
  }
}
